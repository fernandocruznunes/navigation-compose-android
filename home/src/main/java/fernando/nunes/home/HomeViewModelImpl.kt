package fernando.nunes.home

import android.util.Log
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavHostController
import dagger.hilt.android.lifecycle.HiltViewModel
import fernando.nunes.domain.navigation.annotations.AppNavController
import fernando.nunes.domain.navigation.annotations.HomeDestination
import fernando.nunes.domain.navigation.annotations.HomeNavController
import fernando.nunes.domain.navigation.destinations.DestinationConfig
import fernando.nunes.domain.navigation.destinations.Destinations
import fernando.nunes.domain.navigation.destinations.HomeDestinationConfig
import fernando.nunes.domain.viewmodel.ComposeViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModelImpl @Inject constructor(
    savedStateHandle: SavedStateHandle,
    @AppNavController
    private val appNavController: NavHostController,
    @HomeNavController
    private val homeNavController: NavHostController,
    @HomeDestination
    private val homeDestinations: Set<@JvmSuppressWildcards DestinationConfig>
) : ComposeViewModel<HomeState>(
    initialState = {
        val destinations = homeDestinations
            .filterIsInstance<HomeDestinationConfig>()
            .sortedBy {
                it.index
            }
        HomeState(
            destinations = destinations,
            selectedRoute = destinations.first()
        )
    }
), HomeViewModel {

    private val selectedRoute: HomeDestinationConfig
        get() {
            return state.value.destinations.first {
                it.route == homeNavController.currentDestination?.route
            }
        }

    init {
        val id = savedStateHandle.get<String>(Destinations.HomePage.USER_ID_ARGUMENT.name)
        Log.d("Testing", "User id: $id")
        viewModelScope.launch {
            homeNavController.currentBackStackEntryFlow.collectLatest { _ ->
                updateState {
                    it.copy(
                        selectedRoute = selectedRoute
                    )
                }
            }
        }
    }

    override suspend fun goToHomeScreen(route: String) {
        homeNavController.navigate(route) {
            homeNavController.currentDestination?.id?.let {
                popUpTo(it) {
                    saveState = true
                    inclusive = true
                }
            }

            launchSingleTop = true
            restoreState = true
        }
    }

    override suspend fun goToProfileScreen() {
        appNavController.navigate(Destinations.ProfilePage.route)
    }
}