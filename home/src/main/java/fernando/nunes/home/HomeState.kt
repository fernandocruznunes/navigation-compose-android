package fernando.nunes.home

import fernando.nunes.domain.navigation.destinations.HomeDestinationConfig

data class HomeState(
    val destinations: List<HomeDestinationConfig>,
    val selectedRoute: HomeDestinationConfig,
)
