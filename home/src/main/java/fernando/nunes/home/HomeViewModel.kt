package fernando.nunes.home

import fernando.nunes.domain.navigation.destinations.Destinations

interface HomeViewModel {
    suspend fun goToHomeScreen(route: String)
    suspend fun goToProfileScreen()
}