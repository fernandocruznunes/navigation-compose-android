package fernando.nunes.home

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.DateRange
import androidx.compose.material.icons.rounded.Notifications
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.navigation.NamedNavArgument
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoSet
import fernando.nunes.domain.navigation.annotations.AppDestination
import fernando.nunes.domain.navigation.annotations.HomeDestination
import fernando.nunes.domain.navigation.destinations.DestinationConfig
import fernando.nunes.domain.navigation.destinations.Destinations
import fernando.nunes.domain.navigation.destinations.HomeDestinationConfig

@Module
@InstallIn(SingletonComponent::class)
abstract class HomeHiltModule {

    companion object {
        @Provides
        @IntoSet
        @AppDestination
        fun providesHomeDestination(): DestinationConfig {
            return object : DestinationConfig {
                override val route: String = Destinations.HomePage.route
                override val arguments: List<NamedNavArgument> = listOf(
                    Destinations.HomePage.USER_ID_ARGUMENT
                )
                override val content: @Composable () -> Unit = {
                    HomePage.HomePageComposable()
                }
            }
        }

        @Provides
        @IntoSet
        @HomeDestination
        fun providesFeedDestination(): DestinationConfig {
            return object : HomeDestinationConfig {
                override val route: String = Destinations.FeedPage.route
                override val arguments: List<NamedNavArgument> = emptyList()
                override val content: @Composable () -> Unit = {
                    HomePage.FeedScreen()
                }
                override val index: Int = 0
                override val name: String = "Feed"
                override val icon: ImageVector = Icons.Rounded.DateRange
            }
        }

        @Provides
        @IntoSet
        @HomeDestination
        fun providesNotificationsDestination(): DestinationConfig {
            return object : HomeDestinationConfig {
                override val route: String = Destinations.NotificationsPage.route
                override val arguments: List<NamedNavArgument> = emptyList()
                override val content: @Composable () -> Unit = {
                    HomePage.NotificationsScreen()
                }
                override val index: Int = 1
                override val name: String = "Notifications"
                override val icon: ImageVector = Icons.Rounded.Notifications
            }
        }
    }
}