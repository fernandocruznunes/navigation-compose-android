package fernando.nunes.home

import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.consumeWindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import fernando.nunes.domain.navigation.HomeNavHost
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)
object HomePage {

    @Composable
    fun HomePageComposable() {
        val viewModel: HomeViewModelImpl = hiltViewModel()
        val state by viewModel.state.collectAsState()
        val scope = rememberCoroutineScope()

        Scaffold(
            topBar = {
                val title = state.selectedRoute.name

                TopAppBar(
                    title = { Text(title) },
                    actions = {
                        IconButton(
                            onClick = {
                                scope.launch {
                                    viewModel.goToProfileScreen()
                                }
                            }
                        ) {
                            Icon(
                                imageVector = Icons.Filled.Person,
                                contentDescription = "User Profile"
                            )
                        }
                    },
                    colors = TopAppBarDefaults.smallTopAppBarColors(
                        containerColor = MaterialTheme.colorScheme.primaryContainer,
                        titleContentColor = MaterialTheme.colorScheme.onPrimaryContainer,
                        actionIconContentColor = MaterialTheme.colorScheme.onPrimaryContainer
                    ),
                )
            },
            bottomBar = {
                BottomAppBar(
                    modifier = Modifier
                        .height(56.dp)
                ) {
                    state.destinations.forEach {
                        val isSelected = it == state.selectedRoute
                        BottomNavigationItem(
                            icon = {
                                Icon(
                                    imageVector = it.icon,
                                    contentDescription = it.name,
                                )
                            },
                            label = { Text(it.name) },
                            selected = isSelected,
                            onClick = {
                                if (!isSelected) {
                                    scope.launch {
                                        viewModel.goToHomeScreen(it.route)
                                    }
                                }
                            },
                        )
                    }
                }
            },
            containerColor = MaterialTheme.colorScheme.background,
            contentColor = MaterialTheme.colorScheme.onBackground,
        ) {
            HomeNavHost(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(it)
                    .consumeWindowInsets(it)
            )

        }
    }

    @Composable
    fun FeedScreen() {
        val feedData = listOf(
            "2019 Toyota Camry for sale",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "2020 Honda Civic review",
            "Best cars for commuting",
            "Top 10 cars for families"
        )

        LazyColumn {
            items(feedData) { item ->
                Text(
                    text = item,
                    modifier = Modifier.padding(16.dp),
                    style = MaterialTheme.typography.bodyMedium
                )
            }
        }
    }

    @Composable
    fun NotificationsScreen() {
        val notificationData = listOf(
            "New cars for sale",
            "Special offers on used cars",
            "Get a free car wash with your next purchase",
            "Limited time offer: 0% financing"
        )

        LazyColumn {
            items(notificationData) { item ->
                Text(
                    text = item,
                    modifier = Modifier.padding(16.dp),
                    style = MaterialTheme.typography.bodyMedium
                )
            }
        }
    }

}