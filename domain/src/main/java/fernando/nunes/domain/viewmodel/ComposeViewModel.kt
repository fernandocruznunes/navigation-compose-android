package fernando.nunes.domain.viewmodel

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update

abstract class ComposeViewModel<S>(initialState: () -> S): ViewModel() {
    private val _state: MutableStateFlow<S> = MutableStateFlow(initialState())
    val state: StateFlow<S> = _state

    protected fun updateState(function: (S) -> S) {
        _state.update { function(it) }
    }
}