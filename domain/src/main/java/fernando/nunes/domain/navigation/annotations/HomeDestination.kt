package fernando.nunes.domain.navigation.annotations

import javax.inject.Qualifier

@Target(
    AnnotationTarget.TYPE,
    AnnotationTarget.CLASS,
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY,
    AnnotationTarget.VALUE_PARAMETER
)
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class HomeDestination