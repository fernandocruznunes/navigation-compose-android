package fernando.nunes.domain.navigation

import androidx.lifecycle.ViewModel
import androidx.navigation.NavHostController
import dagger.hilt.android.lifecycle.HiltViewModel
import fernando.nunes.domain.navigation.annotations.AppDestination
import fernando.nunes.domain.navigation.annotations.AppNavController
import fernando.nunes.domain.navigation.annotations.HomeDestination
import fernando.nunes.domain.navigation.annotations.HomeNavController
import fernando.nunes.domain.navigation.destinations.DestinationConfig
import javax.inject.Inject

@HiltViewModel
internal class NavigationViewModel @Inject constructor(
    @AppNavController
    val appNavController: NavHostController,
    @HomeNavController
    val homeNavController: NavHostController,
    @AppDestination
    val appDestinations: Set<@JvmSuppressWildcards DestinationConfig>,
    @HomeDestination
    val homeDestinations: Set<@JvmSuppressWildcards DestinationConfig>,
) : ViewModel()