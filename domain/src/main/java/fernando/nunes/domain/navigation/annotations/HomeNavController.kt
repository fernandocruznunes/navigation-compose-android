package fernando.nunes.domain.navigation.annotations

import javax.inject.Qualifier

@Qualifier
annotation class HomeNavController