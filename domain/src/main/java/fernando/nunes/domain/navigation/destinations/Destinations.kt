package fernando.nunes.domain.navigation.destinations

import androidx.navigation.NavType
import androidx.navigation.navArgument

sealed class Destinations {
    abstract val route: String

    object LoginPage : Destinations() {
        override val route: String = "login-page"
    }

    object RegisterPage : Destinations() {
        override val route: String = "register-page"
    }

    object HomePage : Destinations() {
        val USER_ID_ARGUMENT = navArgument("USER_ID") { type = NavType.StringType }
        override val route: String = "home-page/{${USER_ID_ARGUMENT.name}}"
    }

    object FeedPage : Destinations() {
        override val route: String = "feed-page"
    }

    object NotificationsPage : Destinations() {
        override val route: String = "notifications-page"
    }

    object ProfilePage : Destinations() {
        override val route: String = "profile-page"
    }
}