package fernando.nunes.domain.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import fernando.nunes.domain.navigation.destinations.DestinationConfig
import fernando.nunes.domain.navigation.destinations.Destinations

@Composable
fun AppNavHost() {
    val viewModel: NavigationViewModel = hiltViewModel()
    CustomNavHost(
        navHostController = viewModel.appNavController,
        firstDestination = Destinations.LoginPage.route,
        destinations = viewModel.appDestinations
    )
}


@Composable
fun HomeNavHost(
    modifier: Modifier = Modifier
) {
    val viewModel: NavigationViewModel = hiltViewModel()
    CustomNavHost(
        navHostController = viewModel.homeNavController,
        firstDestination = Destinations.FeedPage.route,
        destinations = viewModel.homeDestinations,
        modifier = modifier
    )
}

@Composable
private fun CustomNavHost(
    navHostController: NavHostController,
    firstDestination: String,
    destinations: Set<DestinationConfig>,
    modifier: Modifier = Modifier
) {
    NavHost(
        navController = navHostController,
        startDestination = firstDestination,
        modifier = modifier
    ) {
        destinations.forEach { dest ->
            composable(
                route = dest.route,
                arguments = dest.arguments
            ) {
                dest.content()
            }
        }
    }
}