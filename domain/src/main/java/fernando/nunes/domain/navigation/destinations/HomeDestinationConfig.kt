package fernando.nunes.domain.navigation.destinations

import androidx.compose.ui.graphics.vector.ImageVector

interface HomeDestinationConfig: DestinationConfig {
    val index: Int
    val name: String
    val icon: ImageVector
}
