package fernando.nunes.domain.navigation

import android.content.Context
import androidx.navigation.NavHostController
import androidx.navigation.compose.ComposeNavigator
import androidx.navigation.compose.DialogNavigator
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import fernando.nunes.domain.navigation.annotations.AppNavController
import fernando.nunes.domain.navigation.annotations.HomeNavController
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DomainHiltModule {

    companion object {
        @Provides
        @Singleton
        @AppNavController
        fun providesAppNavController(
            @ApplicationContext context: Context
        ): NavHostController {
            return NavHostController(context).apply {
                navigatorProvider.addNavigator(ComposeNavigator())
                navigatorProvider.addNavigator(DialogNavigator())
            }
        }

        @Provides
        @Singleton
        @HomeNavController
        fun providesHomeNavController(
            @ApplicationContext context: Context
        ): NavHostController {
            return NavHostController(context).apply {
                navigatorProvider.addNavigator(ComposeNavigator())
                navigatorProvider.addNavigator(DialogNavigator())
            }
        }

    }
}