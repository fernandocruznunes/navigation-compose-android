package fernando.nunes.domain.navigation.destinations

import androidx.compose.runtime.Composable
import androidx.navigation.NamedNavArgument

interface DestinationConfig {
    val route: String
    val arguments: List<NamedNavArgument>
    val content: @Composable () -> Unit
}
