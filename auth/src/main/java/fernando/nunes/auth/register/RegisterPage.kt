package fernando.nunes.auth.register

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
object RegisterPage {

    @Composable
    fun RegisterPageComposable() {

        val viewModel: RegisterViewModelImpl = hiltViewModel()

        // State variables to hold the user's name, email, and password values
        var name by remember { mutableStateOf("John Doe") }
        var email by remember { mutableStateOf("johndoe@gmail.com") }
        var password by remember { mutableStateOf("johndoe") }

        val scope = rememberCoroutineScope()

        // UI elements for name, email, and password input fields
        Column(
            modifier = Modifier.padding(16.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            OutlinedTextField(
                value = name,
                onValueChange = { name = it },
                label = { Text("Name") },
                modifier = Modifier.fillMaxWidth()
            )
            OutlinedTextField(
                value = email,
                onValueChange = { email = it },
                label = { Text("Email") },
                modifier = Modifier.fillMaxWidth()
            )
            OutlinedTextField(
                value = password,
                onValueChange = { password = it },
                label = { Text("Password") },
                visualTransformation = PasswordVisualTransformation(),
                modifier = Modifier.fillMaxWidth()
            )

            // UI element for the register button
            Button(
                onClick = {
                    scope.launch {
                        viewModel.goToHomeScreen()
                    }
                },
                modifier = Modifier
                    .fillMaxWidth()


            ) {
                Text("Register")
            }


            // UI element for the register button
            OutlinedButton(
                onClick = {
                    scope.launch {
                        viewModel.goToLoginScreen()
                    }
                },
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Text("Back to Login")
            }
        }


    }

}