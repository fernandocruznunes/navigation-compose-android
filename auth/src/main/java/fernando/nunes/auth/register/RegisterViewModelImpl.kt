package fernando.nunes.auth.register

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.navigation.NavHostController
import dagger.hilt.android.lifecycle.HiltViewModel
import fernando.nunes.domain.navigation.annotations.AppNavController
import fernando.nunes.domain.navigation.destinations.Destinations
import javax.inject.Inject

@HiltViewModel
class RegisterViewModelImpl @Inject constructor(
    @AppNavController
    private val navController: NavHostController
) : ViewModel(), RegisterViewModel {

    override suspend fun goToLoginScreen() {
        navController.navigate(Destinations.LoginPage.route) {
            popUpTo(Destinations.RegisterPage.route) {
                inclusive = true
                saveState =  true
            }
            launchSingleTop = true
            restoreState = true
        }
    }

    override suspend fun goToHomeScreen() {
        navController.navigate(Destinations.HomePage.route) {
            popUpTo(Destinations.RegisterPage.route) {
                inclusive = true
            }
        }
    }

}