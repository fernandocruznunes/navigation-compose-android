package fernando.nunes.auth.register

interface RegisterViewModel {
    suspend fun goToLoginScreen()
    suspend fun goToHomeScreen()
}