package fernando.nunes.auth.login

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import fernando.nunes.domain.navigation.destinations.Destinations
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
object LoginPage {

    @Composable
    fun LoginPageComposable() {

        val viewModel: LoginViewModelImpl = hiltViewModel()

        // State variables to hold the email and password values
        var email by remember { mutableStateOf("johndoe@gmail.com") }
        var password by remember { mutableStateOf("johndoe") }

        val scope = rememberCoroutineScope()

        // UI elements for email and password input fields
        Column(
            modifier = Modifier.padding(16.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            OutlinedTextField(
                value = email,
                onValueChange = { email = it },
                label = { Text("Email") },
                modifier = Modifier.fillMaxWidth()
            )
            OutlinedTextField(
                value = password,
                onValueChange = { password = it },
                label = { Text("Password") },
                visualTransformation = PasswordVisualTransformation(),
                modifier = Modifier.fillMaxWidth()
            )
            Button(
                onClick = {
                    scope.launch {
                        viewModel.goToHomeScreen()
                    }
                },
                modifier = Modifier.fillMaxWidth()
            ) {
                Text("Log in")
            }

            OutlinedButton(
                onClick = {
                    scope.launch {
                        viewModel.goToRegisterScreen()
                    }
                },
                modifier = Modifier.fillMaxWidth()
            ) {
                Text("Register Account")
            }
        }
    }
}