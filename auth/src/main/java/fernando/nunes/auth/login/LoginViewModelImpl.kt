package fernando.nunes.auth.login

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.navigation.NavHostController
import dagger.hilt.android.lifecycle.HiltViewModel
import fernando.nunes.domain.navigation.annotations.AppNavController
import fernando.nunes.domain.navigation.destinations.Destinations
import javax.inject.Inject

@HiltViewModel
class LoginViewModelImpl @Inject constructor(
    @AppNavController
    val navController: NavHostController
) : ViewModel(), LoginViewModel {

    override suspend fun goToRegisterScreen() {
        navController.navigate(Destinations.RegisterPage.route) {
            popUpTo(Destinations.LoginPage.route) {
                inclusive = true
                saveState =  true
            }
            launchSingleTop = true
            restoreState = true
        }
    }

    override suspend fun goToHomeScreen() {
        val route = Destinations.HomePage.route.replace(
            "{${Destinations.HomePage.USER_ID_ARGUMENT.name}}",
            "465"
        )
        navController.navigate(route) {
            popUpTo(Destinations.LoginPage.route) {
                inclusive = true
            }
        }
    }

}