package fernando.nunes.auth.login

interface LoginViewModel {
    suspend fun goToRegisterScreen()
    suspend fun goToHomeScreen()
}