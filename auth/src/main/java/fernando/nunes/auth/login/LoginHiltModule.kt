package fernando.nunes.auth.login

import androidx.compose.runtime.Composable
import androidx.navigation.NamedNavArgument
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoSet
import fernando.nunes.domain.navigation.destinations.DestinationConfig
import fernando.nunes.domain.navigation.annotations.AppDestination
import fernando.nunes.domain.navigation.destinations.Destinations

@Module
@InstallIn(SingletonComponent::class)
abstract class LoginHiltModule {

    companion object {
        @Provides
        @IntoSet
        @AppDestination
        fun providesLoginDestination(): DestinationConfig {
            return object: DestinationConfig {
                override val route: String = Destinations.LoginPage.route
                override val arguments: List<NamedNavArgument> = emptyList()
                override val content: @Composable () -> Unit = {
                    LoginPage.LoginPageComposable()
                }
            }
        }
    }
}