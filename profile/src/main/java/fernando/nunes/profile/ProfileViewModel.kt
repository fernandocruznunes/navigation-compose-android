package fernando.nunes.profile

interface ProfileViewModel {
    suspend fun goBack()
}