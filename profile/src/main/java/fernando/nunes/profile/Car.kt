package fernando.nunes.profile

data class Car(val make: String, val model: String, val year: Int)