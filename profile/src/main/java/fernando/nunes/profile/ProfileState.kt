package fernando.nunes.profile

data class ProfileState(
    val name: String = "John Doe",
    val age: Int = 30,
    val avatarUrl: String = "https://randomuser.me/api/portraits/men/1.jpg",
    val cars: List<Car> = listOf(
        Car("Toyota", "Corolla", 2018),
        Car("Honda", "Civic", 2020),
        Car("Tesla", "Model S", 2021),
    )
)