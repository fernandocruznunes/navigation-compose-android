package fernando.nunes.profile

import androidx.navigation.NavHostController
import dagger.hilt.android.lifecycle.HiltViewModel
import fernando.nunes.domain.navigation.annotations.AppNavController
import fernando.nunes.domain.viewmodel.ComposeViewModel
import javax.inject.Inject

@HiltViewModel
class ProfileViewModelImpl @Inject constructor(
    @AppNavController
    private val navController: NavHostController
) : ComposeViewModel<ProfileState>(
    initialState = {
        ProfileState()
    }
), ProfileViewModel {
    override suspend fun goBack() {
        navController.popBackStack()
    }
}